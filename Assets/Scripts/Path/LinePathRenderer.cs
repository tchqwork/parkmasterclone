﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Класс для представления маршрута через LineRenderer
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class LinePathRenderer : PathRenderer
{
    static private int order = 0;
    [SerializeField] LineRenderer line;
    List<Vector3> pointsList = new List<Vector3>();
    public override int PointsCount { get { return line.positionCount; } }
    public override List<Vector3> Points { get { return pointsList; } }

    void Start()
    {
        if(!line) line = GetComponentInChildren<LineRenderer>(true);
        line.sortingOrder = order++;
    }

    public override void SetColor(Color color)
    {
        color.a = 0.5f;
        if (!line) line = GetComponentInChildren<LineRenderer>(true);
        line.startColor = line.endColor = color;
    }

    public override void SetPoints(List<Vector3> points)
    {
        if (points?.Count > 0)
        {
            pointsList.Clear();
            pointsList.AddRange(points.Select(x => new Vector3(x.x, x.z, 0)));
            if (!line) line = GetComponent<LineRenderer>();
            line.SetVertexCount(pointsList.Count);
            line.SetPositions(pointsList.ToArray());
        }
    }

    public override void Clear()
    {
        pointsList.Clear();
        line.SetVertexCount(pointsList.Count);
    }

    public override void AddPoint(Vector3 point)
    {
        Vector3 rotatePoint = new Vector3(point.x, point.z, 0);
        pointsList.Add(rotatePoint);
        line.SetVertexCount(pointsList.Count);
        line.SetPosition(pointsList.Count - 1, rotatePoint);
    }

    public override void MoveOnTop()
    {
        line.sortingOrder = order++;
    }
}
