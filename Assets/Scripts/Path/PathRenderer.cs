﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Абстрактный класс для представлений рисуемых маршрутов
/// </summary>
public abstract class PathRenderer : MonoBehaviour
{
    public abstract int PointsCount { get; }
    public abstract List<Vector3> Points { get; }
    public abstract void SetColor(Color color);
    public abstract void SetPoints(List<Vector3> points);
    public abstract void AddPoint(Vector3 point);
    public abstract void Clear();
    public abstract void MoveOnTop();
}
