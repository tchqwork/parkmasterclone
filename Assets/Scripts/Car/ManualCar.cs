﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для управляемых игроком машин
/// </summary>
[RequireComponent(typeof(DrawPathController))]
public class ManualCar : BaseCar
{
    override protected void Awake()
    {
        base.Awake();

        pathController.OnPathDrawFinished.AddListener(PathDrawFinished);
        carCollider.isTrigger = false;
    }

    public override void Reset()
    {
        base.Reset();
        pathController.CanDraw = true;
        //GetComponent<BoxCollider>().isTrigger = false;
    }

    public override void SimulationCompleted()
    {
        pathController.CanDraw = true;
    }

    private void PathDrawFinished()
    {
        if (pathController.PointsCount > 0)
        {
            SetPath(pathController.Points);
        }
        else
        {
            SetPath(new List<Vector3>());
        }

        GameController.Instance.DrawPathFinished();
    }
}
