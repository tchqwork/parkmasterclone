﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для бульдозера, который может двигать коробки, но от столкновений тоже должен отлетать
/// </summary>
public class Buldozer : ManualCar
{
    protected override void OnTriggerEnter(Collider other)
    {
        if (!isHit && other.tag == "Car")
        {
            base.OnTriggerEnter(other);
        }
    }
}
