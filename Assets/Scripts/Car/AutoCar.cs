﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Машина с заданным путём, которой мы не управляем
/// </summary>
public class AutoCar : BaseCar
{
    override public void Init(TrafficActorData data)
    {
        base.Init(data);
        var points = data.FixedRoute.Select(x => new Vector3(x.x, 0, x.y)).ToList();
        pathController.SetPoints(points);
        base.SetPath(points);
    }
    override public void SetPath(List<Vector3> path)
    {

    }

    public override void SimulationCompleted()
    {
        pathController.CanDraw = false;
    }
}
