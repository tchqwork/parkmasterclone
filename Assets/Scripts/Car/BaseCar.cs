﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Базовый класс машин на поле
/// </summary>
public abstract class BaseCar : SimulationBehaviour
{
    public int Id { get; protected set; }
    public bool Active { get; protected set; }
    [SerializeField] protected float speed = 1f;
    // Просто, чтобы как-то быстро поменять цвет скачанных машин
    [SerializeField] Renderer mainRenderer;

    protected Collider carCollider;
    protected Tween moveTween = null;
    protected Rigidbody rb;
    protected DrawPathController pathController;

    protected TrafficActorData data;
    protected List<Vector3> path;
    protected bool isHit = false;

    public UnityAction OnMoved;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        pathController = GetComponent<DrawPathController>();
        carCollider = GetComponent<Collider>();
    }
    public virtual void Init(TrafficActorData data)
    {
        Id = data.id;
        this.data = data;

        transform.position = new Vector3(data.startPoint.x, 0, data.startPoint.y);
        SetStartPositionRotation(transform.position, transform.rotation);
        if (this.data.carType == "Player")
        {
            mainRenderer.material.color = this.data.color;
        }
    }

    public void SetColor(Color color)
    {
        if(!pathController) pathController = GetComponent<DrawPathController>();
        pathController.SetColor(color);
    }

    public virtual void SetPath(List<Vector3> path)
    {
        this.path = path;
    }
    public override void StartSimulation()
    {
        base.StartSimulation();
        if (path != null && path.Count > 0)
        {
            pathController.CanDraw = false;
            var tween = transform.DOPath(path.ToArray(), speed).SetEase(Ease.Linear).SetLookAt(0.01f).SetSpeedBased(true).OnComplete(SimulationCompleted);
            //tween.plugOptions.lockRotationAxis = AxisConstraint.Z;
            moveTween = tween;
            OnMoved?.Invoke();
        }
        else
        {
            moveTween?.Kill(false);
        }
    }
    public override void StopSimulation()
    {
        moveTween?.Kill(false);
    }

    public override void Reset()
    {
        base.Reset();
        moveTween.Kill(false);
        rb.isKinematic = true;
        isHit = false;
        Active = true;
        carCollider.isTrigger = true;
    }

    /// <summary>
    /// Столкнулись с машиной или препятствием на сцене, включаем физику и подбрасываем объекты
    /// </summary>
    /// <param name="other"></param>
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (!isHit && other.tag == "Car" || other.tag == "Obstacle")
        {
            Debug.Log($"Trigger {name}:{other.name}");
            rb.isKinematic = false;
            carCollider.isTrigger = false;
            Hit(other.transform.position);
            moveTween.Kill(false);
            if (other.tag == "Obstacle")
            {
                other.attachedRigidbody.AddExplosionForce(500f, transform.position, 100f);
            }
        }
    }

    protected void Hit(Vector3 pos)
    {
        isHit = true;
        Active = false;
        rb.AddExplosionForce(500f, pos, 100f, 10f);
        OnMoved?.Invoke();
    }

    private void OnDestroy()
    {
        moveTween.Kill(false);
        moveTween = null;
    }
}
