﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelManager : SingletonBehaviour<LevelManager>
{
    [SerializeField] Transform LevelRoot;

    public bool IsLoaded { get; private set; }
    public int CurrentLevel { get; private set; }
    List<LevelData> levels = new List<LevelData>();

    private string testLevelDataFile = "leveldata";

    override protected void Awake()
    {
        base.Awake();
        ReadLevelsData();
    }
    /// <summary>
    /// Получаем откуда-то данные об уровнях
    /// </summary>
    private void ReadLevelsData()
    {
        TextAsset levelsData = (TextAsset)Resources.Load(testLevelDataFile, typeof(TextAsset));
        var data = JsonUtility.FromJson<GameLevelsData>(levelsData.text);
        levels = data.levels;
    }

    public ProceduralLevel LoadLevel(int levelNum)
    {
        if (levels.Count > 0)
        {
            var levelPrefab = Resources.Load<ProceduralLevel>("Level");
            var level = Instantiate(levelPrefab, LevelRoot);
            level.name = $"Level_{levelNum}";
            level.Init(levels[levelNum % levels.Count]);

            CurrentLevel++;

            return level;
        }
        else
        {
            Debug.LogError($"No levels loaded!");
        }

        return null;
    }
}
