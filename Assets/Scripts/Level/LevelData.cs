﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Классы для хранения и сериализации данных уровней
[Serializable]
public class TrafficActorData
{
    public int id;
    public bool isFixed;
    public bool mustFinish;
    public List<Vector2> FixedRoute;
    public Vector2 startPoint;
    public Vector2 endPoint;
    public string carType;
    public Color color;
}
[Serializable]
public enum ObstacleType
{
    Box = 0,
}
[Serializable]
public struct Obstacle
{
    public ObstacleType type;
    public Vector2 position;
}
[Serializable]
public class LevelData
{
    public int id;
    public List<TrafficActorData> actors;
    public List<Vector2> levelAreaPoints;
    public List<Vector2> coins;
    public List<Obstacle> obstacles;
}
[Serializable]
public class GameLevelsData
{
    public int version;
    public List<LevelData> levels;
}