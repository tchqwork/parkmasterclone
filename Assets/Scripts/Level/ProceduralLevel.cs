﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Класс для процедурно генерируемого уровня
/// </summary>
public class ProceduralLevel : MonoBehaviour
{
    [SerializeField] Transform GeometryContainer;
    [SerializeField] Transform WallContainer;
    [SerializeField] Transform CoinsContainer;
    [SerializeField] Transform TrafficActorsContainer;
    [SerializeField] Material DefaultMaterial;
    [SerializeField] TrafficActorController TrafficActorPrefab;

    /// <summary>
    /// Список объектов для симуляции происходящего
    /// </summary>
    private List<SimulationBehaviour> actorsInLevel;
    private List<DestinationPointController> destinationPoints;
    private LevelData _data;

    private const string coinPrefabName = "Coin";
    private const string trafficActorPrefabName = "TrafficActor";
    private const string destPrefabName = "DestinationPoint";
    private const string sourcePrefabName = "SourcePoint";
    private const string obstaclePrefabName = "Box";

    private const string GroundLayerName = "GroundLayer";
    private const string WallColliderName = "WallCollider";

    public bool Completed { get; private set; }
    List<Color> colors = new List<Color>() { Color.red, Color.blue, Color.green, Color.magenta, Color.yellow };

    /// <summary>
    /// В данном случае строим геометрию уровня по точкам
    /// </summary>
    private void BuildGeometry()
    {
        Vector3[] vertices;
        int[] triangles;
        Vector2[] uvcoords;

        List<Vector3> points = _data.levelAreaPoints.Select(x => new Vector3(x.x, 0, x.y)).ToList();
        // Делаем триангуляцию по заданным точкам, получаем игровую плоскость
        Triangulation.GetResult(points, true, Vector3.up, out vertices, out triangles, out uvcoords);

        // Область, где могут ездить машины
        var geometry = new GameObject("Level Geometry");
        geometry.transform.parent = GeometryContainer;
        geometry.transform.localPosition = Vector3.zero;
        var mf = geometry.AddComponent<MeshFilter>();
        var mr = geometry.AddComponent<MeshRenderer>();
        mr.sharedMaterial = DefaultMaterial;
        Mesh mesh = new Mesh();
        mf.mesh = mesh;
        mesh.vertices = vertices;
        mesh.uv = uvcoords;
        mesh.triangles = triangles;
        geometry.AddComponent<MeshCollider>();
        geometry.layer = LayerMask.NameToLayer(GroundLayerName);

        // Невидимая стена вокруг уровня для ограничений рисования маршрута
        points.Add(points[0]);
        var wall = new GameObject("Wall");
        wall.transform.parent = WallContainer;
        var wallMeshFilter = wall.AddComponent<MeshFilter>();
        var wallVertices = new Vector3[(points.Count - 1) * 4];
        var wallTris = new int[(points.Count - 1) * 6];

        for (int i = 0; i < points.Count - 1; i++)
        {
            wallVertices[i * 4] = new Vector3(points[i].x, -1, points[i].z);
            wallVertices[i * 4 + 1] = new Vector3(points[i].x, 1, points[i].z);
            wallVertices[i * 4 + 2] = new Vector3(points[i + 1].x, -1, points[i + 1].z);
            wallVertices[i * 4 + 3] = new Vector3(points[i + 1].x, 1, points[i + 1].z);

            wallTris[i * 6] = i * 4;
            wallTris[i * 6 + 1] = i * 4 + 2;
            wallTris[i * 6 + 2] = i * 4 + 1;

            wallTris[i * 6 + 3] = i * 4 + 2;
            wallTris[i * 6 + 4] = i * 4 + 3;
            wallTris[i * 6 + 5] = i * 4 + 1;
        }

        wallMeshFilter.mesh = new Mesh();
        wallMeshFilter.mesh.vertices = wallVertices;
        wallMeshFilter.mesh.triangles = wallTris;
        Vector3[] normals = wallMeshFilter.mesh.normals;
        for (int i = 0; i < normals.Length; i++)
            normals[i] = -normals[i];
        wallMeshFilter.mesh.normals = normals;
        wall.AddComponent<MeshCollider>();
        wall.layer = LayerMask.NameToLayer(WallColliderName);
    }

    /// <summary>
    /// Заполняем уровень игровыми объектами
    /// </summary>
    private void CreateObjects()
    {
        actorsInLevel = new List<SimulationBehaviour>();
        destinationPoints = new List<DestinationPointController>();

        var coinPrefab = Resources.Load<Coin>(coinPrefabName);
        foreach (var c in _data.coins)
        {
            var coin = Instantiate(coinPrefab, new Vector3(c.x, 0, c.y), Quaternion.identity, CoinsContainer);
            actorsInLevel.Add(coin);
            coin.SetStartPositionRotation(new Vector3(c.x, 0, c.y), Quaternion.identity);
        }

        var destPrefab = Resources.Load<DestinationPointController>(destPrefabName);
        var sourcePrefab = Resources.Load<SourcePointController>(sourcePrefabName);
        var obstaclePrefab = Resources.Load<Box>(obstaclePrefabName);

        int n = 0;
        foreach (var actorData in _data.actors)
        {
            Color color = (actorData.isFixed || !actorData.mustFinish) ? Color.grey : colors[n % _data.actors.Count];

            var trafficActor = Instantiate(TrafficActorPrefab, TrafficActorsContainer);
            trafficActor.Init(actorData);
            trafficActor.SetColor(color);

            if (actorData.mustFinish || actorData.isFixed)
            {
                var destController = Instantiate(destPrefab, new Vector3(actorData.endPoint.x, -0.03f, actorData.endPoint.y), Quaternion.identity, TrafficActorsContainer);
                destController.Init(actorData.id);
                destController.SetColor(color);
                destController.OnDestinationReached += DestinationReached;
                trafficActor.Car.OnReset += destController.Activate;
                destinationPoints.Add(destController);
            }

            if (!actorData.isFixed)
            {
                var dc = trafficActor.Car.GetComponent<DrawPathController>();
                dc.CanDraw = true;
                var sourceController = Instantiate(sourcePrefab, new Vector3(actorData.startPoint.x, -0.03f, actorData.startPoint.y), Quaternion.identity, TrafficActorsContainer);
                sourceController.OnSourcePointClicked += ResetSimulation;
                sourceController.OnSourcePointClicked += dc.Reset;
                trafficActor.Car.OnMoved += sourceController.Activate;
                trafficActor.Car.OnReset += sourceController.Deactivate;
                sourceController.SetColor(color);
            }

            actorsInLevel.Add(trafficActor.Car);
            n++;
        }

        foreach (var box in _data.obstacles)
        {
            var boxController = Instantiate(obstaclePrefab, new Vector3(box.position.x, 0, box.position.y), Quaternion.identity, transform);
            boxController.SetStartPositionRotation(new Vector3(box.position.x, 0, box.position.y), Quaternion.identity);
            actorsInLevel.Add(boxController);
        }
    }

    public void Init(LevelData data)
    {
        Completed = false;
        _data = data;

        BuildGeometry();
        CreateObjects();
    }

    public void StartSimulation()
    {
        actorsInLevel.ForEach(x => x.Reset());
        actorsInLevel.ForEach(x => x.StartSimulation());
    }

    public void ResetSimulation()
    {
        actorsInLevel.ForEach(x => x.Reset());
    }

    void DestinationReached()
    {
        if (destinationPoints.All(x => x.Reached))
        {
            Completed = true;
            Invoke("LevelCompleted", 0.5f);
        }
    }

    void LevelCompleted()
    {
        actorsInLevel.ForEach(x => {
            var line = x.GetComponent<DrawPathController>();
            if (line) line.Reset();
            x.StopSimulation();
        });
        GameController.Instance.LevelCompleted();
    }
    /// <summary>
    /// Примерно выставляем камеру, чтобы уровень влез на экран
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="cameraDistance"></param>
    public Vector3 GetCameraDistance(Camera camera, float cameraDistance = 10f)
    {
        var collider = GeometryContainer.GetComponentInChildren<Collider>();
        Vector3 objectSizes = collider.bounds.max - collider.bounds.min;
        float objectSize = Mathf.Max(objectSizes.x, objectSizes.y, objectSizes.z);
        float cameraView = 2.0f * Mathf.Tan(0.5f * Mathf.Deg2Rad * camera.fieldOfView) * 10f;
        float distance = cameraDistance * objectSize / cameraView;
        camera.transform.transform.rotation.SetLookRotation(GeometryContainer.position);
        return collider.bounds.center - distance * camera.transform.forward;
    }
}
