﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Coin : SimulationBehaviour
{
    [SerializeField] int value = 1;

    override public void Reset()
    {
        base.Reset();
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"Coin trigger");
        if (other.tag == "Car")
        {
            gameObject.SetActive(false);
            GameController.Instance.AddCoins(value);
        }
    }
}
