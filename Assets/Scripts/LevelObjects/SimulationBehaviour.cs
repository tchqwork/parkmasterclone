﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Класс объектов для симуляции после создания маршрутов
/// Для простоты записал туда и коробки с монетками, чтобы всё появлялось и становилось на свои места в начале очередного моделирования
/// </summary>
public abstract class SimulationBehaviour : MonoBehaviour
{
    protected Vector3 StartPosition;
    protected Quaternion StartRotarion;

    public Action OnStartSimulation;
    public Action OnCompleteSimulation;
    public Action OnReset;

    public virtual void SetStartPositionRotation(Vector3 pos, Quaternion rot)
    {
        StartPosition = pos;
        StartRotarion = rot;
    }

    public virtual void Reset()
    {
        OnReset?.Invoke();
        transform.position = StartPosition;
        transform.rotation = StartRotarion;
    }

    public virtual void StartSimulation()
    {
        OnStartSimulation?.Invoke();
    }

    public virtual void StopSimulation()
    {

    }

    public virtual void SimulationCompleted()
    {
        OnCompleteSimulation?.Invoke();
    }
}
