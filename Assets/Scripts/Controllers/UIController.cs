﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : SingletonBehaviour<UIController>
{
    [SerializeField] Text CoinsNumber;
    [SerializeField] Text LevelNumber;
    [SerializeField] GameObject WinPanel;
    [SerializeField] GameObject LoadingLevelPanel;

    protected override void Awake()
    {
        base.Awake();

        CoinsNumber.text = "0";
        LevelNumber.text = "Level 1";
    }

    public void UpdateCoinsNumer(int num)
    {
        CoinsNumber.text = num.ToString();
    }

    public void UpdateCurrentLevelInfo(int levelNum)
    {
        LevelNumber.text = $"Level {levelNum}";
    }

    public void LevelCompleted()
    {
        WinPanel.gameObject.SetActive(true);
    }

    public void NextLevel()
    {
        WinPanel.gameObject.SetActive(false);
        GameController.Instance.NextLevel();
    }
}
