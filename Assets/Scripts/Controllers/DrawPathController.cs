﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Контроллер создания пути
/// </summary>
public class DrawPathController : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    public static readonly string PathDrawableLayer = "GroundLayer";
    public static readonly string BackgroundLayer = "Background";
    public static readonly string WallColliderLayer = "WallCollider";

    private int PathDrawableLayerNum;
    private int BackgroundLayerNum;
    private int WallLayerNum;

    public int Id { get; private set; }
    public bool BlockInput = false;

    public UnityEvent OnPathDrawFinished;
    public PathRenderer pathRenderer;
    public bool CanDraw { get; set; }
    public int PointsCount { get { return Points.Count; } }
    public List<Vector3> Points { get; } = new List<Vector3>();

    private Camera mainCamera;
    /// <summary>
    /// Последняя допустимая точка, которую добавляем в маршрут
    /// </summary>
    private Vector3 lastValidPoint = Vector3.zero;
    public void Init(int id)
    {
        Id = id;
    }

    void Awake()
    {
        PathDrawableLayerNum = LayerMask.NameToLayer(PathDrawableLayer);
        BackgroundLayerNum = LayerMask.NameToLayer(BackgroundLayer);
        WallLayerNum = LayerMask.NameToLayer(WallColliderLayer);
        mainCamera = Camera.main;
        lastValidPoint = transform.position;
    }

    public void SetColor(Color color)
    {
        pathRenderer.SetColor(color);
    }

    public void SetPoints(List<Vector3> points)
    {
        if (points?.Count > 0)
        {
            Points.Clear();
            Points.AddRange(points);
            pathRenderer.SetPoints(Points);
        }
    }

    /// <summary>
    /// Непосредственно добавление новых точек пути
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        if (!CanDraw)
            return;

        var Ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit mouseHit;
        if (Physics.Raycast(Ray, out mouseHit, 1000, 1 << PathDrawableLayerNum | 1 << BackgroundLayerNum))
        {
            //Debug.Log("Ray hit, " + mouseHit.collider.name);
            Debug.DrawLine(lastValidPoint - (mouseHit.point - lastValidPoint).normalized * 0.1f, mouseHit.point);

            RaycastHit info;
            // Либо следующую точку мы ставим в рамках игрового поля
            if (!Physics.Linecast(mouseHit.point, lastValidPoint - (mouseHit.point - lastValidPoint).normalized * 0.1f, out info, 1 << WallLayerNum))
            {
                Debug.Log("No wall");
                lastValidPoint = mouseHit.point;
            }
            else
            {
                Debug.Log("Hit wall");
                // Либо наш путь упёрся в стену
                Vector3 dir = new Vector3((mouseHit.point.x - lastValidPoint.x) * Mathf.Abs(info.normal.z),
                                         info.normal.y,
                                         (mouseHit.point.z - lastValidPoint.z) * Mathf.Abs(info.normal.x));
                Vector3 dest = info.point + dir;

                Debug.DrawRay(info.point, dir, Color.red);
                Debug.DrawLine(lastValidPoint, lastValidPoint + 10 * dir, Color.green);

                RaycastHit secondHit;
                // Тонкий момент заключается в ситуации, когда мы тянем путь по краю игрового поля и подходим к точке соединения с другой гранью поля
                if (Physics.Linecast(lastValidPoint + dir, lastValidPoint - (mouseHit.point - lastValidPoint).normalized * 0.1f, out secondHit, 1 << WallLayerNum))
                {
                    dest = secondHit.point;
                }
                //Debug.Log($"{dir}, d={dest}, h={mouseHit.point}, c={info.point}");

                lastValidPoint = dest;
            }
            // Если новая точка достаточно далеко от предыдущей - добавляем в список
            // В теории, после создания пути его было бы неплохо упростить, очистив от лишних точке и т.п., чтобы модель меньше дёргалась при движении
            if (Points.Count == 1 || Vector3.Distance(Points[Points.Count - 1], lastValidPoint) > 0.2f)
            {
                Points.Add(lastValidPoint);
                pathRenderer.AddPoint(Points[Points.Count - 1]);
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (CanDraw)
        {
            OnPathDrawFinished?.Invoke();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (CanDraw)
        {
            lastValidPoint = transform.position;
            Points.Add(transform.position);
            pathRenderer.AddPoint(Points[Points.Count - 1]);
            pathRenderer.MoveOnTop();
        }
    }

    public void Reset()
    {
        Points.Clear();
        pathRenderer.Clear();
    }
}
