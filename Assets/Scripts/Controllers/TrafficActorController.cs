﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Сейчас это контейнер для машины и представления ее маршрута
/// </summary>
public class TrafficActorController : MonoBehaviour
{
    [SerializeField] PathRenderer pathRenderer;
    public BaseCar Car { get; private set; }

    public void Init(TrafficActorData data)
    {
        var carPrefab = Resources.Load<BaseCar>(data.carType);
        Car = Instantiate(carPrefab, transform);
        var pathController = Car.GetComponent<DrawPathController>();
        pathController.pathRenderer = pathRenderer;
        Car.Init(data);
    }

    public void SetColor(Color color)
    {
        Car.SetColor(color);
        pathRenderer.SetColor(color);
    }

    public void StartSimulation()
    {
        /*car.transform.position = sourcePoint.transform.position;
        var line = GetComponentInChildren<LineRenderer>();
        Debug.Log($"Startsimulation {name} {line.positionCount}");
        if (line.positionCount > 0)
        {
            Vector3[] waypoints = new Vector3[line.positionCount];
            line.GetPositions(waypoints);
            var path = new List<Vector3>();
            path.AddRange(waypoints);
            car.SetPath(path);
            car.StartSimulation();
        }*/
    }

    public void StopSimulation()
    {
        //tweener = car.transform.DOMove(sourcePoint.transform.position, 0.5f);
    }
}
