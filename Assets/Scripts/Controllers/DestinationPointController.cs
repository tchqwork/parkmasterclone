﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Класс целевой площадки, куда должны приезжать машины
/// </summary>
public class DestinationPointController : MonoBehaviour
{
    public int Id { get; private set; }
    public bool Reached { get; private set; }
    public UnityAction OnDestinationReached;

    Color activeColor;
    Color reachedColor;
    MeshRenderer mr;
    /// <summary>
    /// В каком радиусе считаем, что машина доехала до конечной точки
    /// </summary>
    [SerializeField] float reachRadius = 0.5f;
    public void Init(int id)
    {
        Id = id;
        Reached = false;
    }
    /// <summary>
    /// Включаем площадку для проведения симуляции
    /// </summary>
    public void Activate()
    {
        Reached = false;
        mr.material.color = activeColor;
    }
    /// <summary>
    /// МАшина доехала - отключаем площадку
    /// </summary>
    public void Deactivate()
    {
        Reached = true;
        mr.material.color = reachedColor;
    }

    public void SetColor(Color color)
    {
        if (!mr) mr = GetComponentInChildren<MeshRenderer>(true);

        mr.material.color = color;
        activeColor = color;
        activeColor.a = 0.5f;
        reachedColor = color;
        mr.material.color = activeColor;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!Reached && other.tag == "Car" && Vector3.Distance(transform.position, other.transform.position) < reachRadius)
        {
            //Debug.Log($"Destination entered: {other.name}");
            var car = other.GetComponent<BaseCar>();
            if (car != null && Id == car.Id && car.Active)
            {
                Deactivate();
                OnDestinationReached?.Invoke();
            }
        }
    }
}
