﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : SingletonBehaviour<GameController>
{
    [SerializeField] UnityEvent OnInitialised;
    /// <summary>
    /// Положение текущего уровня
    /// </summary>
    [SerializeField] Transform CurrentLevelPlace;
    /// <summary>
    /// Положение, куда уезжает пройденный уровень
    /// </summary>
    [SerializeField] Transform OldLevelPlace;
    /// <summary>
    /// Положение, откуда приезжает следующий уровень
    /// </summary>
    [SerializeField] Transform NextLevelPlace;
    /// <summary>
    /// Расстояние до камеры от центра уровня
    /// Используется для автоматического рассчета положения камеры в зависимости от величины уровня
    /// </summary>
    [SerializeField] float cameraDistance = 1f;

    /// <summary>
    /// Всего набрано очков
    /// </summary>
    private int coinsTotal = 0;
    /// <summary>
    /// Сколько набрано очков на этом уровне
    /// </summary>
    private int coinsLevel = 0;
    private int currentLevelNumer = 0;

    /// <summary>
    /// Текущий уровень
    /// </summary>
    ProceduralLevel currentLevel;
    /// <summary>
    /// Следующий уровень, который подгружаем заранее
    /// </summary>
    ProceduralLevel nextLevel;
    Camera mainCamera;

    protected override void Awake()
    {
        base.Awake();
        mainCamera = Camera.main;
    }

    void Start()
    {
        Init();
    }

    void Init()
    {
        // В реальносте где-то здесь получались бы данные по текущему номеру уровеня, очкам и т.д.
        // Что-нибудь вроде currentLevelNumer = dataManager.GetCurrentLevelNumber();
        // Решил уже не заморачиваться
        UIController.Instance.UpdateCurrentLevelInfo(currentLevelNumer + 1);
        currentLevel = LevelManager.Instance.LoadLevel(currentLevelNumer);
        currentLevel.transform.position = CurrentLevelPlace.position;
        mainCamera.transform.DOMove(currentLevel.GetCameraDistance(mainCamera, cameraDistance), 0.3f).SetAutoKill(true);

        OnInitialised?.Invoke();
        StartCoroutine(CreateNextLevel());
    }

    public void Reset()
    {
        coinsLevel = 0;
        UIController.Instance.UpdateCoinsNumer(coinsTotal);
    }

    public void DrawPathFinished()
    {
        coinsLevel = 0;
        UIController.Instance.UpdateCoinsNumer(coinsTotal);
        currentLevel.StartSimulation();
    }

    public void AddCoins(int coinsNum)
    {
        Debug.Log($"{coinsTotal}:{coinsLevel}");
        coinsLevel += coinsNum;
        UIController.Instance.UpdateCoinsNumer(coinsTotal + coinsLevel);
    }

    public void LevelCompleted()
    {
        UIController.Instance.LevelCompleted();
    }
    /// <summary>
    /// Переходим на следующий уровень
    /// </summary>
    public void NextLevel()
    {
        coinsTotal += coinsLevel;
        coinsLevel = 0;
        UIController.Instance.UpdateCoinsNumer(coinsTotal);

        nextLevel.gameObject.SetActive(true);
        currentLevel.transform.DOMove(OldLevelPlace.position, 1f).SetAutoKill(true);
        nextLevel.transform.DOMove(CurrentLevelPlace.position, 1f).OnComplete(() =>
        {
            currentLevel.gameObject.SetActive(false);
            Destroy(currentLevel.gameObject);
            currentLevel = nextLevel;
            currentLevelNumer++;
            mainCamera.transform.DOMove(currentLevel.GetCameraDistance(mainCamera, cameraDistance), 0.5f).SetAutoKill(true);
            UIController.Instance.UpdateCurrentLevelInfo(currentLevelNumer + 1);
            StartCoroutine(CreateNextLevel());
        }).SetAutoKill(true);
    }

    public IEnumerator CreateNextLevel()
    {
        nextLevel = LevelManager.Instance.LoadLevel(currentLevelNumer + 1);
        nextLevel.gameObject.SetActive(false);
        nextLevel.transform.position = NextLevelPlace.position;
        yield return null;
    }
}