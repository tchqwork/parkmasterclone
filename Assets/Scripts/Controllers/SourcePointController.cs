﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Класс для маркера начала пути управляемых машин
/// </summary>
public class SourcePointController : MonoBehaviour, IPointerDownHandler
{
    Vector3 baseScale;
    public UnityAction OnSourcePointClicked;
    MeshRenderer mr;

    private void Awake()
    {
        baseScale = transform.localScale;
        gameObject.SetActive(false);
    }

    public void SetColor(Color color)
    {
        color.a = 0.5f;
        if (!mr) mr = GetComponentInChildren<MeshRenderer>(true);
        mr.material.color = color;
    }

    public void Activate() => gameObject.SetActive(true);
    public void Deactivate() => gameObject.SetActive(false);

    void Update()
    {
        transform.localScale = baseScale * (0.8f + Time.time % 1.2f);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        gameObject.SetActive(false);
        OnSourcePointClicked?.Invoke();
    }
}
