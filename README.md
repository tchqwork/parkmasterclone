# ParkMasterClone

Goal: implement core mechanics of one of the top hyper-casual games.
Park Master - https://apps.apple.com/app/id1481293953

You should implement these mechanics with unity primitives or any assets from the Asset Store.
Only core mechanics, nothing else.

You SHOULD care about
- Game feel
- Control
- Camera
- Animations
- Tactility
- Smoothness of game process
- Clean code

You SHOULD NOT care about
- UI / UX
- Metagame
- Levels
- Architecture
